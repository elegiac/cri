#!/usr/bin/env python3
from enum import Enum

NB_COLORS = 6
NB_TOKENS = 7

class Color(Enum):
    RED = 0
    BLACK = 1
    WHITE = 2
    YELLOW = 3
    BLUE = 4
    GREEN = 5

TOKENS = {
    1: [Color.BLACK, Color.WHITE, Color.GREEN, Color.YELLOW, Color.BLUE, Color.RED],
    2: [Color.RED, Color.WHITE, Color.BLACK, Color.GREEN, Color.BLUE, Color.YELLOW],
    3: [Color.YELLOW, Color.GREEN, Color.RED, Color.WHITE, Color.BLUE, Color.BLACK],
    4: [Color.WHITE, Color.RED, Color.GREEN, Color.YELLOW, Color.BLACK, Color.BLUE],
    5: [Color.BLACK, Color.RED, Color.WHITE, Color.BLUE, Color.YELLOW, Color.GREEN],
    6: [Color.WHITE, Color.GREEN, Color.BLACK, Color.BLUE, Color.YELLOW, Color.RED],
    7: [Color.BLACK, Color.YELLOW, Color.WHITE, Color.BLUE, Color.GREEN, Color.RED]
}

def get_side_colors(token_id, center_color):
    token = TOKENS[token_id]
    for pos, color in enumerate(token):
        if color == center_color:
            return (token[(pos + 1) % NB_COLORS], token[(pos - 1) % NB_COLORS])

def find_matching_token(result, available, prev_right_color):
    print('result =', result)

    if not available:
        print('found result =', result)
        results.append(result.copy())
        return

    for pos in range(NB_TOKENS):
        if not result[pos]:
            print('filling position', pos, 'with available tokens', available)

            if (pos > 0):
                next_center_color = TOKENS[result[0]][pos % NB_COLORS]
                print('next center color =', next_center_color)
 
            for a in available:
                current_side_colors = (None, None)

                if (pos > 0):
                    current_side_colors = get_side_colors(a, TOKENS[result[0]][pos - 1])
                    print('right color for', a, '=', current_side_colors[1])
                    if (next_center_color == current_side_colors[1]):
                        continue

                    if (pos == 6):
                        pos1_left_color = get_side_colors(result[1], TOKENS[result[0]][0])[0]
                        print('pos1_left_color =', pos1_left_color)
                        if current_side_colors[1] != pos1_left_color:
                            continue

                if (pos > 1):
                    print('left color for', a, '=', current_side_colors[0])
                    if (prev_right_color != current_side_colors[0]):
                        continue

                print('found matching token', a, 'at position', pos)
                result[pos] = a
                next_available = set(available)
                next_available.remove(a)
                find_matching_token(result, next_available, current_side_colors[1])
                continue
            
            print('no match found here')
            result[pos] = None
            return

results = []
result = [None] * NB_TOKENS
available = set(range(1, 8))

find_matching_token(result, available, None)

print('\nresults =', results)
